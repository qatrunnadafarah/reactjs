const express = require("express");
const cors = require("cors");
const jwt = require("jsonwebtoken");
const app = express();

const routes = require('./routes/index');
const { urlencoded } = require("express");


// const corsOptions ={
//   origin:'http://localhost:3000', 
//   credentials:true,            //access-control-allow-credentials:true
//   optionSuccessStatus:200
// }

// app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.use(routes);

app.listen(3001, () => {
  console.log("Listening on port", 3001);
});
