const { Cat } = require('../models');

module.exports = {
    login(req, res) {
        const token = jwt.sign({ id: 1, email: req.body.email }, "Rahasia");
        res.status(201).json({ token });
    },

    getAll(req, res) {
        Cat.findAll()
            .then(cats => {
                res.status(200).json(cats);
            })
            .catch(err => {
                res.send(err);
            })
    },

    getById(req, res) {
        // Cat.findByPk(req.params.id)
        //     .then(cat => {
        //         res.status(200).json(cat);
        //     })
        //     .catch(err => {
        //         res.send(err);
        //         console.log(err.message);
        //     })
        const item = Cat.findByPk(req.params.id);
        return res.send(item);
    },

    delete(req, res) {
        Cat.findByPk(req.params.id);
        Cat.destroy({
            where: {
                id: req.params.id
            }
        })
            .then(cat => {
                res.status(200).json(cat);
            })
            .catch(err => {
                res.send(err);
            })
    },

    create(req, res) {
        console.log(req.body, "ini body");
        Cat.create(req.body)
            .then(cat => {
                res.status(201).json({
                    status: "CREATED",
                    data: cat,
                });

            })
            .catch(err => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    update(req, res) {
        Cat.update(req.body, {
            where: {
                id: req.params.id
            }
        })
            .then(cat => {
                res.status(200).json(cat);
            })
            .catch(err => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            })
    },


}