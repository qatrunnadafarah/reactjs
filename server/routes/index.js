const express = require('express');
const router = express.Router();

const catController = require('../controllers/catController');

router.post('/profile', catController.create);
router.get('/profile', catController.getAll);
router.get('/profile/:id', catController.getById);
router.put('/profile/:id', catController.update);
router.delete('/profile/:id', catController.delete);


router.post("/login", catController.login)

module.exports = router;