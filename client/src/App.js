import { Navbar, Nav, Container } from 'react-bootstrap'
import { Home, Profile, Login } from './pages';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import Protected from './components/Protected';
import AddForm from './components/AddForm';
// import {} from '@react-oauth/google';

function App() {
  return (
    <BrowserRouter>
      <Navbar bg="dark" variant="dark" className='navbar'>
        <Container>
          <Navbar.Brand to="/">SEOUL</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href='/'>Home</Nav.Link>
            <Nav.Link href='/profile'>Profile</Nav.Link>
            <Nav.Link href='/login'>Login</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/login" element={<Login />} />
        <Route exact path="/profile" element={
          <Protected>
            <Profile />
          </Protected>
        } />
        <Route exact path="/profile/create" element={
          <Protected>
            <AddForm />
          </Protected>
        } />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
