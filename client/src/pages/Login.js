import { Form, Button, Container } from 'react-bootstrap';
import { useEffect, useState } from 'react';
// import { GoogleOAuthProvider } from '@react-oauth/google'
// import style from "./Loading.module.css";
import { GoogleLogin } from '@react-oauth/google';
import * as firebase from 'firebase/app';
import 'firebase/auth';

const Login = () => {
    const [email, setEmail] = useState('');
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const token = localStorage.getItem('token');

    useEffect(() => {
        // document.title = "Login Page"
        token ? setIsLoggedIn(true) : setIsLoggedIn(false)
    }, [token]);

    useEffect(() => {
        console.log('coba');
    }, [isLoggedIn])

    const handleLogin = (e) => {
        e.preventDefault();
        fetch('http://localhost:3001/api/v1/auth/login', {
            method: 'POST',
            body: { email }
        }).then((response) => {
            console.log();
            return response.json()
        }).then((result) => {
            localStorage.setItem('token', result.token);
            console.log(token);
        })
    }

    const handleLogout = (e) => {
        setIsLoggedIn(false)
        localStorage.removeItem('token')
    }

    // const onSuccess = (responseGoogle) => {
    //     console.log("Login succes", responseGoogle.profileObj);
    //     localStorage.setItem("token", JSON.stringify(responseGoogle.profileObj));
    //     setIsLoggedIn(true);
        // navigate("/login");
    // };

    return (
        <Container>
            {!isLoggedIn ? (
                <div>
                    <Form onSubmit={handleLogin}>
                        <Form.Group className='mb-3' controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" onChanges={(e) => {
                                setEmail(e.target.value)
                            }} values={email} />
                            <Form.Text className="text-muted">
                                We'll never share your email to anyone else.
                            </Form.Text>
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>

                    <GoogleLogin
                        onSuccess={credentialResponse => {
                            console.log(credentialResponse);
                            localStorage.setItem('token', credentialResponse.credential)
                            setIsLoggedIn(true);
                        }}
                        onError={() => {
                            console.log('Login Failed');
                        }}
                    />
                </div>
            ) : (
                <Button variant="primary" onClick={handleLogout}>
                    Logout
                </Button>
            )}

            <div className='footer-regist'>
                <h3>03177 서울특별시 종로구 새문안로 55 서울역사박물관 02-724-0274~6</h3>
                <p>COPYRIGHT(C) 2021. SEOUL MUSEUM OF HISTORY ALL RIGHTS RESERVED</p>
            </div>
        </Container>
    )
}

export default Login;