import { Container, Card, ListGroup } from 'react-bootstrap';
import "antd/dist/antd.css";
import { useState, useEffect } from 'react';
import { Link } from "react-router-dom"
import axios from 'axios';

function Profile() {
    const [data, setData] = useState([]);
    const [error, setError] = useState(null);
    const [isChange, setIsChange] = useState(false);
    const token = localStorage.getItem("token");
    // state = {
    //     cats: []
    // };

    // const apiGet = () => {
    //     fetch('http://localhost:3001/profile', {
    //         method: "POST",
    //         headers: {
    //             "Content-Type": "application/x-www-form-urlencoded"
    //         }
    //     })
    //         .then((response) => response.json())
    //         .then((json) => {
    //             console.log(json);
    //             setData(json);
    //         })
    // }

    // const apiGet = () => {
    //     axios.get('http://localhost:3001/profile')
    //         .then(res => {
    //             const cats = res.data;
    //             this.setState({ cats });
    //         })
    //     }

    // useEffect(() => apiGet(), []);


    useEffect(() => {
        axios
            .get("http://localhost:3001/profile")
            .then(res => {setData(res.data)})
            .catch(err => {setError(err.message)});
    }, [isChange, token]);

    const onDelete = async (e) => {
        setIsChange(true);
        await axios.delete(`http://localhost:3001/profile/${e.target.value}`);
        setIsChange(false);
      };

    return (
        <div>
            <Container>
                <div className="d-flex justify-content-end">
                    <Link to="/profile/create">
                        <button className="btn btn-primary">Add new item</button>
                    </Link>
                </div>
                <div>
                    <ul>
                        {data &&
                            data.map(cat => (
                                <Card style={{ width: '18rem' }}>
                                    <Card.Body>
                                        <Card.Title>{cat?.name}</Card.Title>
                                        <ListGroup variant="flush">
                                            <ListGroup.Item>Age: {cat?.age}</ListGroup.Item>
                                            <ListGroup.Item>Breed: {cat?.breed}</ListGroup.Item>
                                        </ListGroup>
                                        <div className="col-md-6 d-flex-justify-content-center">
                                            <button value={cat.id} onClick={onDelete} className="btn btn-danger">
                                                Delete
                                            </button>
                                        </div>
                                    </Card.Body>
                                </Card>
                            ))}
                    </ul>
                </div>
            </Container>

            <div className='footer'>
                <h3>03177 서울특별시 종로구 새문안로 55 서울역사박물관 02-724-0274~6</h3>
                <p>COPYRIGHT(C) 2021. SEOUL MUSEUM OF HISTORY ALL RIGHTS RESERVED</p>
            </div>

        </div>
    )
}

export default Profile;

// function Profile() {
//     const apiGet = () {
//         fetch('')
//         .then((response) => response.json())
//         .then((json) => console.log(json))
//     }

//     return (
//         <div>
//             <Container>
//                 <Carousel>
//                     <Carousel.Item>
//                         <img
//                             className="d-block w-100"
//                             src="new_york.jpg"
//                             alt="First slide"
//                         />
//                         <Carousel.Caption>
//                             <h3>First slide label</h3>
//                             <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
//                         </Carousel.Caption>
//                     </Carousel.Item>
//                     <Carousel.Item>
//                         <img
//                             className="d-block w-100"
//                             src="building.jpg"
//                             alt="Second slide"
//                         />

//                         <Carousel.Caption>
//                             <h3>Second slide label</h3>
//                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
//                         </Carousel.Caption>
//                     </Carousel.Item>
//                     <Carousel.Item>
//                         <img
//                             className="d-block w-100"
//                             src="korea.jpg"
//                             alt="Third slide"
//                         />

//                         <Carousel.Caption>
//                             <h3>Third slide label</h3>
//                             <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
//                         </Carousel.Caption>
//                     </Carousel.Item>
//                 </Carousel>
//             </Container>

//             <div className='footer'>
//                 <h3>03177 서울특별시 종로구 새문안로 55 서울역사박물관 02-724-0274~6</h3>
//                 <p>COPYRIGHT(C) 2021. SEOUL MUSEUM OF HISTORY ALL RIGHTS RESERVED</p>
//             </div>

//         </div>
//     )
// }

// export default Profile;