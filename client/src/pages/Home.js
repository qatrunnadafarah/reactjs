import { Card, } from 'react-bootstrap';
import "antd/dist/antd.css";

function Home() {
    return (
        <div>
            <Card className="bg-dark text-white">
                <Card.Img src="jeju.jpg" alt="Card image" />
                <Card.ImgOverlay>
                    <Card.Title>GLIMPSE OF KOREA</Card.Title>
                    <Card.Text>
                        "You can always find your SEOULmate here."
                    </Card.Text>
                </Card.ImgOverlay>
            </Card>

            <div className='footer'>
                <h3>03177 서울특별시 종로구 새문안로 55 서울역사박물관 02-724-0274~6</h3>
                <p>COPYRIGHT(C) 2021. SEOUL MUSEUM OF HISTORY ALL RIGHTS RESERVED</p>
            </div>

        </div>
    )
}

export default Home;