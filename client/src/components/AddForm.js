import { useState } from "react";
import { Form, Button, Container } from 'react-bootstrap';
import axios from 'axios';
import { useNavigate } from "react-router-dom";

function AddForm() {
    // const data = { name, age, breed };
    const navigate = useNavigate();
    const [
        name, setName,
        age, setAge,
        breed, setBreed,
    ] = useState("");

    const onSubmit = async (e) => {
        e.preventDefault();
        const datas = { name, age, breed };
        await axios.post("http://localhost:3001/profile", datas);
        navigate("/profile");
    };

    // useEffect(() => {
    //     document.title = "Shop";
    //   }, []);

    return (
        <Container>
        <Form onSubmit={onSubmit}>
            <Form.Group className='mb-3' controlId="formBasicEmail">
                <Form.Label>Nama</Form.Label>
                <Form.Control type="text" placeholder="Enter name" onChanges={(e) => {
                    setName(e.target.value)
                }}  />

                <Form.Label>Age</Form.Label>
                <Form.Control type="integer" placeholder="Enter age" onChanges={(e) => {
                    setAge(e.target.value)
                }}  />

                <Form.Label>Breed</Form.Label>
                <Form.Control type="text" placeholder="Enter breed" onChanges={(e) => {
                    setBreed(e.target.value)
                }}  />
            </Form.Group>
            <Button variant="primary" type="submit">
                Submit
            </Button>
        </Form>
        </Container>
    )
}

export default AddForm;